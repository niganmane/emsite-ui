
import request from '@/router/axios'
const auth_basic = 'Basic YXBwOmFwcA=='
export const loginByUsername = (username, password, code, randomStr) => {
  var grant_type = 'password'
  var scope = 'server'
  return request({
    url: '/uaa/oauth/token',
    headers: {
      'Authorization': auth_basic
    },
    method: 'post',
    params: { username, password, randomStr, code, grant_type, scope }
  })
}

export function mobileLogin(mobile, code) {
  var grant_type = 'mobile'
  var scope = 'server'
  return request({
    url: '/uaa/mobile/token',
    headers: {
      'Authorization': auth_basic
    },
    method: 'post',
    params: { mobile, code, grant_type, scope }
  })
}

export const getUserInfo = () => {
  return request({
    url: '/upms/user/info',
    method: 'get'
  })
}

export const logout = (accesstoken, refreshToken) => {
  return request({
    url: '/uaa/authentication/removeToken',
    method: 'post',
    params: { accesstoken, refreshToken }
  })
}
