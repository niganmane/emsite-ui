import request from '@/router/axios'
export function GetMenu(query) {
  return request({
    url: '/upms/menu/userMenu',
    method: 'get',
    params: query
  })
}
export function fetchTree(query) {
  return request({
    url: '/upms/menu/allTree',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/upms/menu/',
    method: 'post',
    data: obj
  })
}

export function getObj(id) {
  return request({
    url: '/upms/menu/' + id,
    method: 'get'
  })
}

export function delObj(id) {
  return request({
    url: '/upms/menu/' + id,
    method: 'delete'
  })
}

export function putObj(obj) {
  return request({
    url: '/upms/menu/',
    method: 'put',
    data: obj
  })
}
