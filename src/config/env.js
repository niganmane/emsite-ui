/**
 * 配置编译环境和线上环境之间的切换
 *
 * baseUrl: 老项目域名地址
 * khglUrl: 客户管理域名地址
 * dicUrl : 字典服务器地址
 * routerMode: 路由模式
 * imgBaseUrl: 图片所在域名地址
 * welUrl :默认欢迎页
 *
 */
let baseUrl = '';
let iconfontVersion = ['alifont01'];
let iconfontUrl = `/static/cdn/iconfont/$key/iconfont.css`;
let codeUrl = `/upms/code`
if (process.env.NODE_ENV == 'development') {
  baseUrl = `http://127.0.0.1:18763/`;
} else if (process.env.NODE_ENV == 'production') {
  baseUrl = ``;
}

export {
  baseUrl,
  iconfontUrl,
  iconfontVersion,
  codeUrl
}
