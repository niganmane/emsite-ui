const DIC = {
  vaild: [{
    label: '否',
    value: 'false'
  },
    {
      label: '是',
      value: 'true'
    }
  ]
}
export const tableOption = {
  "border": true,
  "index": true,
  "stripe": true,
  "expand": true,
  "menuAlign": "center",
  "align": "center",
  "indexLabel": "序号",
  "searchSize": 'small',
  "editBtn": false,
  "delBtn": false,
  "dic": [],
  "column": [{
    label: "客户端",
    prop: "clientId",
    align: 'center',
    search: true,
    sortable: true,
    rules: [{
      required: true,
      message: "请输入clientId",
      trigger: "blur"
    }]
  }, {
    label: "密钥",
    prop: "clientPwd",
    align: 'center',
    overHidden: true,
    rules: [{
      required: true,
      message: "请输入clientSecret",
      trigger: "blur"
    }]
  }, {
    label: "域",
    prop: "scope",
    align: 'center',
    rules: [{
      required: true,
      message: "请输入scope",
      trigger: "blur"
    }]
  }, {
    label: "授权模式",
    prop: "authorizedGrantTypes",
    align: 'center',
    width: 400,
    hide: true,
    rules: [{
      required: true,
      message: "请输入授权模式",
      trigger: "blur"
    }]
  }, {
    label: "回调地址",
    prop: "webServerRedirectUri",
    align: 'center',
    hide: true,
  }, {
    label: "权限",
    prop: "authorities",
    align: 'center',
    hide: true,
  }, {
    label: "请求令牌",
    prop: "accessTokenValidity",
    align: 'center',
    hide: true,
  }, {
    label: "刷新令牌",
    prop: "refreshTokenValidity",
    align: 'center',
    hide: true,
  }, {
    label: "扩展信息",
    prop: "additionalInformation",
    align: 'center',
    hide: true,
  }, {
    label: "自动放行",
    prop: "autoapprove",
    align: 'center',
    type: 'radio',
    dicData:DIC.vaild,
    rules: [{
      required: true,
      message: "请选择是否放行",
      trigger: "blur"
    }]
  },{
    label: "资源ID",
    prop: "resourceIds",
    align: 'center',
    hide: true,
  }]
}
