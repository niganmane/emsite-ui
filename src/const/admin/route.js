
const DIC = {
  vaild: [{
      label: '否',
      value: '0'
    },
    {
      label: '是',
      value: '1'
    }
  ]
}
export const tableOption = {
  "border": true,
  "index": true,
  "indexLabel": "序号",
  "stripe": true,
  "menuAlign": "center",
  "align": "center",
  "addBtn": false,
  "editBtn": false,
  "delBtn": false,
  "showClomnuBtn": false,
  "searchSize": 'small',
  "menuWidth": 200,
  "dic": [],
  "column": [{
    label: "服务名称",
    prop: "serviceId",
    align: 'center',
    sortable: true,
    search: true,
    fixed: true,
    rules: [{
      required: true,
      message: "请输入服务名称",
      trigger: "blur"
    }]
  }, {
    label: "匹配路径",
    prop: "path",
    align: 'center',
  }, {
    label: "转发地址",
    prop: "url",
    align: 'center',
    overHidden: true
  }, {
    label: "去掉前缀",
    prop: "stripPrefix",
    align: 'center',
    type: 'radio',
    dicData: DIC.vaild,
    rules: [{
      required: true,
      message: "是否去掉前缀",
      trigger: "blur"
    }]
  }, {
    label: "是否重试",
    prop: "retryable",
    align: 'center',
    type: 'radio',
    dicData: DIC.vaild,
    rules: [{
      required: true,
      message: "是否重试",
      trigger: "blur"
    }]
  }, {
    label: "是否启用",
    prop: "enabled",
    align: 'center',
    type: 'radio',
    dicData: DIC.vaild,
   rules: [{
     required: true,
     message: "是否启用",
     trigger: "blur"
   }]
  }, {
    label: "敏感头",
    prop: "sensitiveheadersList",
    align: 'center',
    overHidden: true
  }, {
    label: "创建时间",
    prop: "createTime",
    format: 'YYYY-MM-DD hh:mm:ss',
    valueFormat: 'YYYY-MM-DD hh:mm:ss',
    type: 'date',
    align: 'center',
    editVisdiplay: false,
    addVisdiplay: false,
  }]
}
