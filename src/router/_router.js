const _import = require('./_import')
import Layout from '@/page/index/'
export default [{
  path: '/',
  name: '主页',
  redirect: '/dashboard'
}, {
  path: '/dashboard',
  component: Layout,
  redirect: '/dashboard/index',
  children: [{
    path: 'index',
    name: '首页',
    component: _import('dashboard/index', 'views')
  }]
}, {
  path: '*',
  redirect: '/404',
  hidden: true
}, {
  path: '/login',
  name: '登录页',
  component: _import('login/index')
}, {
  name: '锁屏页',
  path: '/lock',
  component: _import('lock/index')
}, {
  path: '/info',
  component: Layout,
  redirect: '/info/index',
  children: [{
    path: 'index',
    name: '修改信息',
    component: _import('admin/user/info', 'views')
  }]
}, {
  path: '/404',
  component: _import('error-page/404', 'components'),
  name: '404'
}, {
  path: '/403',
  component: _import('error-page/403', 'components'),
  name: '403'
}, {
  path: '/500',
  component: _import('error-page/500', 'components'),
  name: '500'
}, {
  path: '/emsiteiframe',
  component: Layout,
  redirect: '/emsiteiframe',
  meta: {
    keepAlive: true
  },
  children: [{
    path: ':routerPath',
    name: 'iframe',
    component: _import('iframe/main', 'components'),
    props: true
  }]
}]
